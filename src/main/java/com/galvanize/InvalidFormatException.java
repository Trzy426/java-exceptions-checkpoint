package com.galvanize;

class InvalidFormatException extends Exception {
    public InvalidFormatException(String message) {
        super(message);
    }

}
