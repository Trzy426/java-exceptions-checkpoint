package com.galvanize;

class NoServiceException extends Exception {
    public NoServiceException(String message) {
        super(message);
    }
}
