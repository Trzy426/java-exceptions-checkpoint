package com.galvanize;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ZipCodeProcessorTest {

    private Verifier verifier;
    private ZipCodeProcessor zipCodeProcessor;

    @BeforeEach
    public void setUp() {
        verifier = new Verifier();
        zipCodeProcessor = new ZipCodeProcessor(verifier);
    }

    // write your tests here
    @Test
    public void thankYouMessageTest() {
        //arrange
        //act
        String actual = zipCodeProcessor.process("76230");
        String expected = "Thank you!  Your package will arrive soon.";
        //assert
        assertEquals(expected, actual);
    }

    //error message for length being too long
    @Test
    public void inputTooLongTest() {
        String actual = zipCodeProcessor.process("123456");
        String expected = "The zip code you entered was the wrong length.";

        assertEquals(expected, actual);
    }

    @Test
    public void inputTooShortTest() {
        String actual = zipCodeProcessor.process("1234");
        String expected = "The zip code you entered was the wrong length.";

        assertEquals(expected, actual);
    }

    @Test
    public void noServiceTest() {
        String actual = zipCodeProcessor.process("10012");
        String expected = "We're sorry, but the zip code you entered is out of our range.";

        assertEquals(expected, actual);
    }
    @Test
    public void VerefierNoServiceTest() {
        //assert
       NoServiceException exception = assertThrows(NoServiceException.class,()-> {
            verifier.verify("12345");
        });
       assertEquals("ERRCODE 27: NO_SERVICE",exception.getMessage());
    }
    public void VerefierTooShortTest() {
        //assert
        NoServiceException exception = assertThrows(NoServiceException.class,()-> {
            verifier.verify("1234");
        });
        assertEquals("ERRCODE 22: INPUT_TOO_SHORT",exception.getMessage());
    }
    public void VerefierTooLongTest() {
        //assert
        NoServiceException exception = assertThrows(NoServiceException.class,()-> {
            verifier.verify("123444");
        });
        assertEquals("ERRCODE 21: INPUT_TOO_LONG",exception.getMessage());
    }


}